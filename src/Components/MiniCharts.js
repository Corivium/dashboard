import React from 'react';

export class MiniChart extends React.Component {

    constructor() {
        super();
        this.state = {
            items : []
        };
    }

    getData = arg => {
        // google sheets data
        const arr = this.state.items;
        const arrLen = arr.length;
        let purchaseRate = 0;
        for (let i = 0; i < arrLen; i++) {
            if (arg === arr[i]["month"]) {
                purchaseRate += parseInt(arr[i].purchase_rate / 3);
            }
        }
        // setting state
        this.setState({
            purchaseRate: purchaseRate
        });
    };

    render() {
        return (
            <div className="row">
                <div className="col-md-4 col-lg-3 light-text mb-4">
                    <div className="card grid-card card-dark">
                        <div className="card-heading mb-3">
                            <div className="dark-text-light letter-spacing text-small">
                                {/* kpi layout as in previous step */}
                                KPI
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-8 col-lg-9 light-text mb-4">
                    <div className="card grid-card card-dark">
                        <div className="row full-height">
                            {/* row to include all mini-charts */}
                            <div className="col-sm-4 full-height">
                                <div className="chart-container">
                                    <div className="dark-text-light letter-spacing text-small">
                                        {/* chart will come here */}
                                        test
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 full-height">
                                <div className="chart-container">
                                    <div className="dark-text-light letter-spacing text-small">
                                        {/* chart will come here */}
                                        test
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4 full-height">
                                <div className="chart-container">
                                    <div className="dark-text-light letter-spacing text-small">
                                        {/* chart will come here */}
                                        test
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}