import React from 'react';
import Nav from 'react-bootstrap/Nav';

export const Navbar = () => {

        return (
            <div id="navigation">
                {/*static navbar top */}
                <Nav className="navbar navbar-expand-lg fixed-top nav-light nav-dark-text">
                    <div className="navbar-brand h1 mb-0 text-large">
                    Online Retail Dashboard
                    </div>
                    <div className="navbar-nav ml-auto">
                    <div className="user-detail-section">
                        <span className="pr-2">Hi, User</span>
                        <span className="img-container">
                            {/* add image uri here */}
                            <img src="" className="rounded-circle" alt="user" />
                        </span>
                        </div>
                    </div>
                </Nav>

                {/*static navbar bottom */}
                <Nav className="navbar fixed-top nav-secondary nav-dark nav-light-text"s>
                    <div className="navbar-brand h1 mb-0 text-medium">
                    Summary
                    </div>
                    <div className="navbar-nav ml-auto">
                    <div className="user-detail-section">
                        <span className="img-container">
                            {/* add dropdown filter menu here */}
                        </span>
                        </div>
                    </div>
                </Nav>
            </div>
        );

}