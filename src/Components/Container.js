import React from 'react';
import { KPI } from './KPI';
import { MiniChart } from './MiniCharts';
import { Charts } from './Charts';



export const ContainerBlock = (props) => {

    return (
        <div id="info-block" className="container-fluid pr-5 pl-5 pt-5 pb-5">
            {/* KPI Section */}
            <KPI getData={props.getData}/>

            {/* KPI + Mini Charts Section */}
            <MiniChart />

            {/* Charts Section */}
            <Charts />

        </div>
    );

}