import React from 'react';


export class KPI extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    getData = arg => {
        // get google sheet data
        const arr = this.state.items;
        const arrLen  = arr.length;
        
        // kpi's

        // get amazon revenue
        let amRevenue = 0;

        // get ebay revenue
        let ebRevenue = 0;

        // get etsy revenue
        let etRevenue = 0;

        for (let i = 0; i < arrLen; i++) {
            if (arg === arr[i]["month"]) {
                if (arr[i]["source"] === "AM") {
                    amRevenue += parseInt(arr[i].revenue);
                }
            } else if (arg === arr[i]["month"]) {
                if (arr[i]["source"] === "EB") {
                    ebRevenue += parseInt(arr[i].revenue);
                }
            } else if (arg === arr[i]["month"]) {
                if (arr[i]["source"] === "ET") {
                    etRevenue += parseInt(arr[i].revenue);
                }
            }
        }

        // set state
        this.setState({
            amRevenue: amRevenue,
            ebRevenue: ebRevenue,
            etRevenue: etRevenue
        });
    
    };

    render() {
        return(
            <div className="row">
                <div className="col-lg-3 col-sm-6 light-text mb-4">
                    <div className="card grid-card card-dark">
                    <div className="card-heading">
                        <div className="dark-text-light letter-spacing text-small">
                        Total Revenue
                        </div>
                    </div>
                    <div className="card-value">
                        <span>$</span>
                        {this.state.amRevenue}
                    </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6 light-text mb-4">
                    <div className="card grid-card card-dark">
                    <div className="card-heading">
                        <div className="dark-text-light letter-spacing text-small">
                        Revenue from Amazon
                        </div>
                        <div className="card-heading-brand"><i className="fab fa-amazon"></i></div>
                    </div>
                    <div className="card-value">
                        <span>$</span>
                        {this.state.amRevenue}
                    </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6 light-text mb-4">
                    <div className="card grid-card card-dark">
                    <div className="card-heading">
                        <div className="dark-text-light letter-spacing text-small">
                        Revenue from Ebay
                        </div>
                    </div>
                    <div className="card-value">
                        <span>$</span>
                        {this.state.ebRevenue}
                    </div>
                    </div>
                </div>
                <div className="col-lg-3 col-sm-6 light-text mb-4">
                    <div className="card grid-card card-dark">
                    <div className="card-heading">
                        <div className="dark-text-light letter-spacing text-small">
                        Revenue from Etsy
                        </div>
                    </div>
                    <div className="card-value">
                        <span>$</span>
                        {this.state.etRevenue}
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}