import React from 'react';

export const Charts = () => {
    
    return (
        <div className="row">
            <div className="col-md-6">
                <div className="card card-dark">
                <div className="chart-div"></div>
                {/* chart will come here */}
                chart 1
                </div>
            </div>
            <div className="col-md-6">
                <div className="card card-dark">
                <div className="chart-div"></div>
                {/* chart will come here */}
                chart 2
                </div>
            </div>
        </div>
    );
}