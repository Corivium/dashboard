import React from 'react';
import config from './config';
import { Navbar } from './Components/Nav';
import { ContainerBlock } from './Components/Container';


const url = `https://sheets.googleapis.com/v4/spreadsheets/${ config.spreadsheetId }/values:batchGet?ranges=Sheet1&majorDimension=ROWS&key=${ config.apiKey }`;

export class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }




  componentDidMount() {
    fetch(url).then(Response => Response.json()).then(data => {
      let batchRowValues = data.valueRanges[0].values;

      const rows = [];
      for (let i = 1; i < batchRowValues.length; i++) {
        let rowObject = {};
        for (let j = 0; j < batchRowValues[i].length; j++) {
          rowObject[batchRowValues[0][j]] = batchRowValues[i][j];
        }
        rows.push(rowObject);
      }

      this.setState(
        { 
          items: rows 
        }
      );

    }); 
  }

  render() {
    return (
      <div>
          <Navbar />
          <ContainerBlock /> 
      </div>
    );
  }


}
